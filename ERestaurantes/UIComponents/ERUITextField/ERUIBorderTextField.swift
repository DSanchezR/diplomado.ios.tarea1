//
//  ERUIBorderTextField.swift
//  ERestaurantes
//
//  Created by David Gregori Sánchez Rafael on 21/02/21.
//

import UIKit

@IBDesignable class ERUIBorderTextField: UITextField, ERBorder {
    
    //MARK: - BORDER
    
     var borderCustomStyle: ERBorderStyle = ERBorderStyle() {
        didSet { self.updateBorderApperence() }
    }
    
    @IBInspectable internal var borderColor: UIColor {
        get { self.borderCustomStyle.color }
        set { self.borderCustomStyle.color = newValue}
    }
    
    @IBInspectable internal var borderWidth: CGFloat {
        get { self.borderCustomStyle.width }
        set { self.borderCustomStyle.width = newValue}
    }
    
}
