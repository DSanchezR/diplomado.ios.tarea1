//
//  ERUIShapeLabel.swift
//  ERestaurantes
//
//  Created by David Gregori Sánchez Rafael on 21/02/21.
//

import UIKit

@IBDesignable class ERUIShapeLabel: ERUIBorderLabel, ERShape {
    
    //MARK: - SHAPE
    
    var shapeStyle: ERShapeStyle = ERShapeStyle(){
        didSet { self.updateShapeApperence() }
    }
    
    @IBInspectable internal var cornerRadius: CGFloat{
        get { self.shapeStyle.radius }
        set { self.shapeStyle.radius = newValue}
    }
    
    @IBInspectable internal var topLeft: Bool{
        get { self.shapeStyle.topLeft }
        set { self.shapeStyle.topLeft = newValue}
    }
    
    @IBInspectable internal var topRigth: Bool{
        get { self.shapeStyle.topRigth }
        set { self.shapeStyle.topRigth = newValue}
    }
    
    @IBInspectable internal var downLeft: Bool{
        get { self.shapeStyle.downLeft }
        set { self.shapeStyle.downLeft = newValue}
    }
    
    @IBInspectable internal var downRigth: Bool{
        get { self.shapeStyle.downRigth }
        set { self.shapeStyle.downRigth = newValue}
    }
    
}
