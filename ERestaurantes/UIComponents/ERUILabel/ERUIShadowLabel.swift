//
//  ERUIShadowLabel.swift
//  ERestaurantes
//
//  Created by David Gregori Sánchez Rafael on 21/02/21.
//

import UIKit

@IBDesignable class ERUIShadowLabel: ERUIShapeLabel, ERShadow {
    
    //MARK: - SHADOW
    
    var shadowStyle: ERShadowStyle = ERShadowStyle(){
        didSet { self.updateShadowApperence() }
    }
    
    @IBInspectable internal override var shadowColor: UIColor? {
        get { self.shadowStyle.color }
        set { self.shadowStyle.color = newValue ?? UIColor.clear }
    }
    
    @IBInspectable internal var shadowRadius: CGFloat{
        get { self.shadowStyle.radius }
        set { self.shadowStyle.radius = newValue}
    }
    
    @IBInspectable internal var shadowOpacity: Float {
        get { self.shadowStyle.opacity }
        set { self.shadowStyle.opacity = newValue}
    }
    
    @IBInspectable internal override var shadowOffset: CGSize {
        get { self.shadowStyle.offset }
        set { self.shadowStyle.offset = newValue}
    }
    
}
