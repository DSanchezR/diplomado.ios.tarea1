//
//  ERUIBorder.swift
//  ERestaurantes
//
//  Created by David Gregori Sánchez Rafael on 20/02/21.
//

import UIKit

struct ERBorderStyle {
    
    var color: UIColor
    var width: CGFloat
    
    init(color: UIColor = .clear, width: CGFloat = 0) {
        
        self.color = color
        self.width = width
    }
}

protocol ERBorder {
    
    // ESTA PROPIEDAD LA VAMOS A USAR DESDE EL CODIGO
    var borderCustomStyle: ERBorderStyle { get set }
    
    // ESTAS PROPIEDADES SOLO LAS VAMOS A USAR DESDE EL STORYBOARD
     var borderColor: UIColor { get set }
     var borderWidth: CGFloat { get set }
    
}

extension ERBorder where Self: UIView {

    func updateBorderApperence() {
        self.layer.borderColor = self.borderCustomStyle.color.cgColor
        self.layer.borderWidth = self.borderCustomStyle.width
    }

}
